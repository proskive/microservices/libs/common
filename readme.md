# ProSkive.Lib.Common

.NET Standard 2.0 Library including common code for ProSkive Services.

## Features

 - Simple JWT Configuration via appsettings
 - Simple Netflix Eureka Configuration via appsettings
 - Simple Health and Info Endpoints via Steeltoe
    - JWT health contributor

## Usage of JWT

For using the JWT support you have to edit `Startup.cs` and `appsettings.json`:

#### Startup.cs

```c#
public void ConfigureServices(IServiceCollection services)
{
    ...
    // Jwt
    services.AddJwt(Configuration);
    ...
}


public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
{
    ...
    // Auth
    app.UseAuthentication();
    ...
}
```

#### appsettings.json

```json
"Jwt": {
    "Audience": {
        "Validate": false,
        "Value": "proskive-name-service"
    },
    "Authority": "http://localhost:8040/auth/realms/UCT",
    "RequireHttps": false
},
```

## Usage of Netflix Eureka

For Eureka support, change `Startup.cs` and `appsettings.json`. It is based on the awesome [Steeltoe library](https://steeltoe.io/).
Eureka will only work, if `eureka__enabled` is set to `true`.

#### Startup.cs

```c#
public void ConfigureServices(IServiceCollection services)
{
    ...
    // Eureka
    services.AddEureka(Configuration);
    ...
}


public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
{
    ...
    // Use Eureka
    app.UseEureka(Configuration);
    ...
}
```

#### appsettings.json

```json
"eureka": {
    "enabled": true,
    "client": {
      "shouldRegisterWithEureka": true,
      "shouldFetchRegistry": false,
      "serviceUrl": "http"
    },
    "instance": {
      "appName": "NOTIFICATION-SERVICE",
      "vipAddress": "notification-service",
      "secureVipAddress": "notification-service",
      "hostname": "localhost",
      "port": 9000,
      "preferIpAddress": false
    }
},
```

For more information about the configuration of eureka visit the [Steeltoe documentation](https://steeltoe.io/docs/steeltoe-discovery/#1-2-2-eureka-client-settings).

## Usage of Health and Info Endpoints

[Steeltoe](https://steeltoe.io/) provides support for health and info endpoints. You can simply use them by changing the `Startup.cs`:

#### Startup.cs

```c#
public void ConfigureServices(IServiceCollection services)
{
    ...
    // Health and Info Endpoints
    services.AddHealthAndInfoEndpoints(Configuration);
    ...
}


public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
{
    ...
    // Management
    app.UseHealthAndInfoEndpoints();
    ...
}
```

#### appsettings.json

It is possible to populate the info endpoint via `appsettings.json`. Simply add more Key-Value-Pairs to the "Info" section:

```json
"Info": {
    "Name": "notification-service"
},
```

## License (GPLv3)
```
ProSkive
Copyright (C) 2018  Medical Informatics Group (MIG) Frankfurt

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```
