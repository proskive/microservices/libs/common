﻿// ProSkive
// Copyright (C) 2018  Medical Informatics Group (MIG) Frankfurt
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Net.Http;
using Microsoft.Extensions.Options;
using ProSkive.Lib.Common.Settings.Snapshots;
using Steeltoe.Management.Endpoint.Health;

namespace ProSkive.Lib.Common.Contributors.Health
{
    public class JwtAuthHealthContributor : IHealthContributor
    {
        public string Id { get; } = "auth";
        private readonly JwtOptionsSnapshot settings;

        public JwtAuthHealthContributor(IOptionsSnapshot<JwtOptionsSnapshot> optionsSnapshot)
        {
            settings = optionsSnapshot.Value;
        }
        
        public Steeltoe.Management.Endpoint.Health.Health Health()
        {
            var health = new Steeltoe.Management.Endpoint.Health.Health();
            
            health.Details.Add("authority", settings.Jwt.Authority);            
            
            var status = CheckHealth();
            health.Details.Add("status", status.ToString());
            health.Status = status;
            
            return health;
        }

        HealthStatus CheckHealth()
        {
            var requestUri = new Uri(settings.Jwt.Authority + "/.well-known/openid-configuration");

            try
            {
                using (var client = new HttpClient())
                {
                    var response = client.GetAsync(requestUri).Result;
                    response.EnsureSuccessStatusCode();
                }
            }
            catch (HttpRequestException e)
            {
                return HealthStatus.DOWN;
            }
            catch (Exception e)
            {
                return e.InnerException is HttpRequestException ? HealthStatus.DOWN : HealthStatus.UNKNOWN;
            }

            return HealthStatus.UP;
        }

    }
}