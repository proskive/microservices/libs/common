﻿namespace ProSkive.Lib.Common.Settings
{
    public class JwtSettings
    {
        public AudienceSettings Audience { get; set; }
        public string Authority { get; set; }
        public bool RequireHttps { get; set; }
    }
    
    public class AudienceSettings
    {
        public bool Validate { get; set; }
        public string Value { get; set; }
    }
}