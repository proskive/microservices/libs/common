﻿namespace ProSkive.Lib.Common.Settings.Snapshots
{
    public class JwtOptionsSnapshot
    {
        public JwtSettings Jwt { get; set; }
    }
}