﻿// ProSkive
// Copyright (C) 2018  Medical Informatics Group (MIG) Frankfurt
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace ProSkive.Lib.Common.Services.ServiceHost
{
    public class ProSkiveServiceHost : IServiceHost
    {
        private readonly IWebHost webHost;

        public ProSkiveServiceHost(IWebHost webHost)
        {
            this.webHost = webHost;
        }
        
        public void Run()
        {
            webHost.Run();
        }

        public static ProSkiveHostBuilder Create<TStartup>(string[] args) where TStartup : class
        {
            Console.Title = typeof(TStartup).Namespace;
            var webhostBuilder = WebHost.CreateDefaultBuilder(args)
                .UseStartup<TStartup>();
            
            return new ProSkiveHostBuilder(webhostBuilder.Build());
        }
        
    }
}