﻿// ProSkive
// Copyright (C) 2018  Medical Informatics Group (MIG) Frankfurt
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using ProSkive.Lib.Common.Contributors.Health;
using ProSkive.Lib.Common.Settings.Snapshots;
using Steeltoe.Discovery.Client;
using Steeltoe.Management.Endpoint.Health;
using Steeltoe.Management.Endpoint.Info;

namespace ProSkive.Lib.Common.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddJwt(this IServiceCollection services, IConfiguration config)
        {
            // Add Options
            services.Configure<JwtOptionsSnapshot>(config);
            
            // Add Health Contributor
            services.AddScoped<IHealthContributor, JwtAuthHealthContributor>();
            
            // Use Jwt
            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = config.GetValue<bool>("Jwt:RequireHttps");
                    options.Audience = config.GetValue<string>("Jwt:Audience:Value");
                    options.Authority = config.GetValue<string>("Jwt:Authority");
                    options.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateAudience = config.GetValue<bool>("Jwt:Audience:Validate")
                    };
                });
        }
        
        public static void AddEureka(this IServiceCollection services, IConfiguration config)
        {
            var eurekaEnabled = config.GetSection("eureka").GetValue<bool>("enabled");
            if (eurekaEnabled)
                services.AddDiscoveryClient(config);
        }

        public static void AddHealthAndInfoEndpoints(this IServiceCollection services, IConfiguration config)
        {
            // Config 
            services.Configure<InfoOptionsSnapshot>(config);
            
            // Health Endpoint
            services.AddHealthActuator(config);
            
            // Info Endpoint
            services.AddInfoActuator(config);
        }
    }
}