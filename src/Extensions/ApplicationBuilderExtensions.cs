﻿// ProSkive
// Copyright (C) 2018  Medical Informatics Group (MIG) Frankfurt
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Steeltoe.Discovery.Client;
using Steeltoe.Management.Endpoint.Health;
using Steeltoe.Management.Endpoint.Info;

namespace ProSkive.Lib.Common.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static void UseEureka(this IApplicationBuilder app, IConfiguration config)
        {
            var eurekaEnabled = config.GetSection("eureka").GetValue<bool>("enabled");
            if (eurekaEnabled)
                app.UseDiscoveryClient();
        }

        public static void UseHealthAndInfoEndpoints(this IApplicationBuilder app)
        {
            app.UseHealthActuator();
            app.UseInfoActuator();
        }
    }
}